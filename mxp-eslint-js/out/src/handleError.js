const hx = require("hbuilderx")
const pluginName = require("../../package.json").name;
const path = require('path');
const nls = require("hxnls");
const localize = nls.loadMessageBundle(__filename);
// 处理错误
async function printError(err) {
	//  创建hx底部问题提示
	let errText = localize(0,null, pluginName)
	hx.window.setStatusBarMessage(errText, 2000, "error");
	let view = hx.window.createOutputChannel("eslint-vue");
	const stack = err.stack.split(/(\r?\n)|\r/);// console.log(stack);
	// view.show();
	for(var i=0,len=stack.length; i < len; i++) {
		if(/\S/.test(stack[i])) {
			let lineContent = {}
			lineContent.line =  stack[i];
			lineContent.level = "error";
			if(i === 0) {
				var isPath = path.isAbsolute(stack[i]);
				if(isPath) {
					const lastIndex = stack[i].lastIndexOf(":");
					const len = stack[i].length;
					const filePath = stack[i].substr(0, lastIndex);
					const lineNum = parseInt(stack[i].substr(lastIndex+1, len)) - 1;
					lineContent.line = filePath;
					lineContent.hyperlinks =  [
							{
								linkPosition: {
									start: 0,
									end: stack[i].length
								},
								onOpen: async function(){
									var documentPromise = await hx.workspace.openTextDocument(filePath);
									if(documentPromise) {
										let activeEditor = await hx.window.getActiveTextEditor();
										activeEditor.gotoLine(lineNum);
									}
								}
							}
					];
				}
			}
			await view.appendLine(lineContent);
		}
	}
}
module.exports = {
	printError
}