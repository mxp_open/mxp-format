const hx = require('hbuilderx');
const WorkspaceEdit = hx.WorkspaceEdit;
const TextEdit = hx.TextEdit;
const eslint = require('eslint');
const fs = require('fs');
const diff = require('./diff.js');
let baseConfig = require('../.eslintrc.js');
const pluginName = require('../package.json').name;
const nls = require('hxnls');
const localize = nls.loadMessageBundle(__filename);
const _ = require('lodash');
const { printError } = require('./src/handleError');
let extensionDir = undefined;
let overrideConfigFile = undefined;
function resolveSettings(doc, _autofix) {
  let autofix = _autofix;
  const userConfigFile = `${hx.env.appData}/extensions/mxp-eslint-js/.eslintrc.js`;
  if (fs.existsSync(userConfigFile)) {
    delete require.cache[require.resolve(userConfigFile)];
    baseConfig = require(userConfigFile);
  }

  // TODO 这里应该用workspace.getWorkspaceFolder API
  const workspaceFolder = doc.workspaceFolder;
  const contents = doc.getText();
  if (contents.length > 512 * 1024 * 1024) {
    // 如果大于512kb，则禁用autofix
    autofix = false;
  }
  const supportLanguages = ['typescript', 'javascript', 'javascript_es6'];
  let CLIEngine = eslint.CLIEngine;
  let cwd = extensionDir || process.cwd();
  let ESlint = eslint.ESLint;
  let ignorePath = null;
  let resolvePluginsRelativeTo = extensionDir;
  if (workspaceFolder && workspaceFolder.uri) {
    const folderUri = workspaceFolder.uri;
    const folderPath = typeof folderUri === 'string' ? folderUri : folderUri.fsPath;
    const userEslint = `${folderPath}/node_modules/eslint`;
    if (fs.existsSync(userEslint)) {
      CLIEngine = require(userEslint).CLIEngine;
      ESlint = require(userEslint).ESLint;
      if (CLIEngine !== undefined || ESlint !== undefined) {
        if (ESlint) {
          cwd = folderPath;
          resolvePluginsRelativeTo = folderPath;
          const enableHTMLScriptValidate = fs.existsSync(`${userEslint}-plugin-html`);
          if (!enableHTMLScriptValidate) {
            cwd = extensionDir;
          } else {
            if (fs.existsSync(userConfigFile)) {
              overrideConfigFile = userConfigFile;
            }
            const userIgnore = `${folderPath}/.eslintignore`;
            if (fs.existsSync(userIgnore)) {
              ignorePath = userIgnore;
            }
          }
        } else {
          // let text = localize("resolveSettings.hx.window.showErrorMessage", "当前项目下使用的eslint版本过低，请使用不低于7.0.0版本的eslint。");
          // hx.window.showErrorMessage(text);
          // CLIEngine = eslint.ESLint;

          const enableHTMLScriptValidate = fs.existsSync(`${userEslint}-plugin-html`);
          if (!enableHTMLScriptValidate) {
            cwd = extensionDir;
            ESlint = eslint.ESLint;
            CLIEngine = undefined;
            ignorePath = null;
          } else {
            if (fs.existsSync(userConfigFile)) {
              overrideConfigFile = userConfigFile;
            }
            const userIgnore = `${folderPath}/.eslintignore`;
            if (fs.existsSync(userIgnore)) {
              ignorePath = userIgnore;
            }
          }
        }
      } else {
        cwd = extensionDir || folderPath;
        const enableHTMLScriptValidate = fs.existsSync(`${userEslint}-plugin-html`);
        if (!enableHTMLScriptValidate) {
          baseConfig.plugins = [];
        }
      }
    }
  }
  if (baseConfig.plugins.indexOf('html') > -1) {
    supportLanguages.push('html');
    supportLanguages.push('html_es6');
  }
  return {
    library: {
      CLIEngine,
    },
    ESlint,
    workingDirectory: cwd,
    options: {
      baseConfig,
      fix: autofix,
      ignorePath,
      overrideConfigFile,
      resolvePluginsRelativeTo,
    },
    supportLanguages,
  };
}
// eslint-disable-next-line consistent-return
function withCLIEngine(callback, settings) {
  const cwd = process.cwd();
  const newOptions = settings.options;
  try {
    if (settings.workingDirectory) {
      settings.options.ignorePath = null;
      newOptions.cwd = settings.workingDirectory;
    }
    const cli = new settings.library.CLIEngine(newOptions);
    return callback(cli);
  } catch (err) {
    hx.window.clearStatusBarMessage();
    // let output = hx.window.createOutputChannel(pluginName);
    // let text = localize("withCLIEngine.output.appendLine","[{0}]验证器内部出现了错误：",pluginName);
    // output.appendLine(text + e.message);
    printError(err);
    if (cwd !== process.cwd()) {
      process.chdir(cwd);
    }
  }
}
// eslint-disable-next-line consistent-return
function withESlint(callback, settings) {
  const cwd = process.cwd();
  const newOptions = settings.options;
  try {
    if (settings.workingDirectory) {
      newOptions.cwd = settings.workingDirectory;
    }
    const eslint1 = new settings.ESlint(newOptions);
    return callback(eslint1);
  } catch (err) {
    hx.window.clearStatusBarMessage();
    // let output = hx.window.createOutputChannel(pluginName);
    // let text = localize("withCLIEngine.output.appendLine","[{0}]验证器内部出现了错误：",pluginName);
    // output.appendLine(text + e.message);
    printError(err);
    if (cwd !== process.cwd()) {
      process.chdir(cwd);
    }
  }
}
function validateAndFixDocument(doc, autofix) {
  const text = doc.getText();
  const settings = resolveSettings(doc, autofix);
  if (settings.supportLanguages.indexOf(doc.languageId) < 0) {
    return;
  }
  const text1 = localize(0, null, pluginName);
  hx.window.setStatusBarMessage(text1);
  if (settings.ESlint) {
    withESlint(async (_eslint) => {
      let isPathIgnore = '';
      const options = {
        filePath: doc.fileName,
        warnIgnored: true,
      };
      try {
        isPathIgnore = await _eslint.isPathIgnored(doc.fileName);
        if (isPathIgnore) {
          const statusMsg = localize(1, null);
          hx.window.setStatusBarMessage(statusMsg, 5000, 'warn');
          return;
        }
        const results = await _eslint.lintText(text, options);
        const errorMsgs = [];
        if (results.length > 0) {
          for (let i = 0; i < results.length; i++) {
            const result = results[i];
            if (result.errorCount > 0) {
              const messages = result.messages;
              if (messages && messages.length > 0) {
                for (let j = 0; j < messages.length; j++) {
                  const errMsg = messages[j];
                  // eslint-disable-next-line max-depth
                  if (Number(errMsg.severity) === 2) {
                    errorMsgs.push({
                      column: errMsg.column - 1,
                      line: errMsg.line - 1,
                      message: errMsg.message,
                    });
                  }
                }
              }
            }
            if (result.output !== undefined) {
              const diffs = diff.stringDiff(text, result.output, false);
              const workspaceEdit = new WorkspaceEdit();
              const edits = [];
              for (const df of diffs) {
                edits.push(
                  new TextEdit(
                    {
                      start: df.originalStart,
                      end: df.originalStart + df.originalLength,
                    },
                    result.output.substr(df.modifiedStart, df.modifiedLength)
                  )
                );
              }
              workspaceEdit.set(doc.uri, edits);
              hx.workspace.applyEdit(workspaceEdit);
            }
          }
          const diagCollection = hx.languages.createDiagnosticCollection(pluginName);
          diagCollection.set(doc.uri, errorMsgs);
        }
        let level = 'info';
        const text2 = localize(2, null, pluginName, errorMsgs.length);
        const text3 = localize(3, null);
        let statusMsg = text2;
        let showtime = 5 * 1000;
        if (errorMsgs.length > 0) {
          level = 'error';
          statusMsg += text3;
          showtime = 30 * 1000;
        }
        hx.window.setStatusBarMessage(statusMsg, showtime, level);
      } catch (err) {
        printError(err);
      }
    }, settings);
    return;
  }
  withCLIEngine((cli) => {
    const report = cli.executeOnText(text, doc.fileName);
    const errorMsgs = [];
    if (report && report.results && Array.isArray(report.results) && report.results.length === 1) {
      const result = report.results[0];
      const messages = result.messages;
      if (messages && messages.length > 0) {
        for (let j = 0; j < messages.length; j++) {
          const errorMsg = messages[j];
          if (Number(errorMsg.severity) === 2) {
            errorMsgs.push({
              column: errorMsg.column - 1,
              line: errorMsg.line - 1,
              message: errorMsg.message,
            });
          }
        }
      }
      const diagCollection = hx.languages.createDiagnosticCollection(pluginName);
      diagCollection.set(doc.uri, errorMsgs);
      // 自动修复
      if (result.output !== undefined) {
        const diffs = diff.stringDiff(text, result.output, false);
        const workspaceEdit = new WorkspaceEdit();
        const edits = [];
        for (const df of diffs) {
          edits.push(
            new TextEdit(
              {
                start: df.originalStart,
                end: df.originalStart + df.originalLength,
              },
              result.output.substr(df.modifiedStart, df.modifiedLength)
            )
          );
        }
        workspaceEdit.set(doc.uri, edits);
        hx.workspace.applyEdit(workspaceEdit);
      }
    }
    let level = 'info';
    const text2 = localize(4, null, pluginName, errorMsgs.length);
    const text3 = localize(5, null);
    let statusMsg = text2;
    let showtime = 5 * 1000;
    if (errorMsgs.length > 0) {
      level = 'error';
      statusMsg += text3;
      showtime = 30 * 1000;
    }
    hx.window.setStatusBarMessage(statusMsg, showtime, level);
  }, settings);
}
function activate(context) {
  const appRoot = hx.env.appRoot;
  const pluginVuePath = `${appRoot}/plugins/mxp-eslint-js/`;
  extensionDir = context.extensionPath || pluginVuePath;
  const autoFixCommandDispose = hx.commands.registerCommand('extension.eslint.autoFix', () => {
    const editorPromise = hx.window.getActiveTextEditor();
    editorPromise.then((editor) => {
      const doc = editor.document;
      try {
        validateAndFixDocument(doc, false);
      } catch (err) {
        printError(err);
      }
    });
  });
  const eslintConfig = hx.workspace.getConfiguration('mxp-eslint-js');
  // 这个是执行保存格式化的时候用的
  const willSaveTextDocumentEventDispose = hx.workspace.onWillSaveTextDocument((event) => {
    const autoFixOnSave = eslintConfig.get('autoFixOnSave', true);
    try {
      validateAndFixDocument(event.document, autoFixOnSave);
    } catch (err) {
      //  创建hx底部问题提示
      printError(err);
    }
  });

  function didChange(event) {
    const validateOnDocumentChanged = eslintConfig.get('validateOnDocumentChanged');
    console.log('validateOnDocumentChanged', validateOnDocumentChanged);
    if (validateOnDocumentChanged) {
      validateAndFixDocument(event.document, false);
    }
  }

  const didChangeTextDocumentEventDispose = hx.workspace.onDidChangeTextDocument(_.debounce(didChange, 300));
  // let didChangeTextDocumentEventDispose = hx.workspace.onDidChangeTextDocument(function (event) {
  //   let validateOnDocumentChanged = eslintConfig.get('validateOnDocumentChanged');
  //   if (validateOnDocumentChanged) {
  //     validateAndFixDocument(event.document, false);
  //   }
  // });
  context.subscriptions.push(autoFixCommandDispose);
  context.subscriptions.push(willSaveTextDocumentEventDispose);
  context.subscriptions.push(didChangeTextDocumentEventDispose);
}
module.exports = {
  activate,
};
