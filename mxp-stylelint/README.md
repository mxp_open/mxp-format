# mxp-stylelint

## 说明

主要功能如下：

1. 使用 stylelint-order 对 css 属性(如：width，height，padding，margin)进行排序 (可禁用，默认启用，启用后需要注意 css 中的条件编译)
2. 支持保存时自动格式化
3. 添加大量规则（加入了几乎所有规则，老项目建议屏蔽部分规则，新项目建议使用默认规则）

【注意】本插件启用了严格的命名，所以可能导致很多报错解决方式就是关闭对应规则如:

```js
{
  // 开启规则
  'selector-class-pattern': /(^[a-z][a-z0-9]*(-[a-z0-9]+)*$)|(^el-[a-z-_]+[a-z]$)/, // class的命名规范

  // 关闭规则方式1 注意关闭规则 null
  'selector-class-pattern': [null, /(^[a-z][a-z0-9]*(-[a-z0-9]+)*$)|(^el-[a-z-_]+[a-z]$)/], // class的命名规范
  // 关闭规则方式2 注意关闭规则是使用 null 而不是false
  'selector-class-pattern': null, // class的命名规范
}
```

## 使用说明

### 格式化所有文件

```json
{
  "name": "mxp",
  "version": "1.0.0",
  "description": "",
  "main": "main.js",
  "scripts": {
    "install": "npm install",
    "prettier": "prettier --write ./**/*.{js,json,vue,ts,jsx,cjs,mjs,tsx,cts,mts,wxss,wxml,wxs}", // 使用prettier格式化所有文件
    "eslint": "eslint . --ext .vue,.js,.jsx,.cjs,.mjs,.ts,.tsx,.cts,.mts --fix", // 使用eslint格式化所有文件
    "stylelint": "stylelint **/*.{css,scss,vue} --fix" // 使用stylelint格式化所有文件
  },
  "devDependencies": {
    "eslint-plugin-html": "^7.1.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint": "^8.34.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint-plugin-jsdoc": "^40.0.0", // 用于eslint格式化jsdoc注释的 （如不需要可以不安装）
    "eslint-plugin-vue": "^9.9.0", // 用于eslint格式化vue （如不需要可以不安装）
    "postcss": "^8.4.21", // 用于stylelint格式化scss或者less建议安装 （如不需要可以不安装）
    "postcss-html": "^1.5.0", // 用于stylelint格式化html或者vue文件中的样式 （如不需要可以不安装）
    "prettier": "^2.8.4", // prettier 代码格式
    "sass": "^1.58.0", //  用于stylelint格式化(防止出错建议需要stylelint的也安装上)  （如不需要可以不安装）
    "stylelint": "15.1.0", //用于stylelint格式化 （如不需要可以不安装）
    "stylelint-config-html": "^1.1.0", // 用于stylelint格式化html或者vue文件中的样式（如不需要可以不安装）
    "stylelint-config-standard-scss": "^7.0.0", // stylelint的scss格式化规则 （如不需要可以不安装）
    "stylelint-order": "^6.0.2" // stylelint的scss属性排序规则 （如不需要可以不安装）
  }
}
```

1. 在根目录中新建 package.json，并加入上述 json 内容
2. 运行 -> 运行到终端 -> install （安装环境 如已安装请忽略）
3. 运行 -> 运行到终端 -> stylelint （开始使用 stylelint 格式化所有文件）

【vscode】：vscode 中怎么使用大家应该都知道，所以不做说明。

### 保存是自动格式化

1. 确保在 工具 -> 设置 -> 插件配置 -> mxp-stylelint 中已勾选保存时自动格式化
2. 打开 html、vue、nvue、css、less、scss、sass 等文件
3. 随便修改 css 样式后保存，就会触发此插件

### 在鼠标右键菜单中使用

1. 安装插件后，打开 html、vue、nvue、css、less、scss、sass 等文件
2. 在上述文件中点击鼠标右键(一定要在上述文件中点击鼠标右键，不要在工程目录中点击)
3. 在弹出菜单中选择 `使用mxp-stylelint格式化css样式` 即可对样式进行自动格式化

## 配置文件与修改规则

本插件使用`.stylelintrc.js`作为`stylelint`的配置文件，如果项目根目录中存在`.stylelintrc.js`文件，以项目中的配置为准，如不存在则使用本插件自带的`.stylelintrc.js`文件。

修改本插件自带配置文件方式如下（建议把自带的配置文件拷贝出来放到项目根目录下方便 vscode 使用，如不使用 vscode 则无需拷贝）：

点击 工具 -> 设置 -> 插件配置 -> mxp-stylelint -> .stylelintrc.js，即可打开配置文件。

## 启用或者禁用属性排序规则

由于 stylelint-order 进行排序的时候可能会影响在 css 中的条件编译，所以本插件支持禁用排序规则
禁用方式如下：

1. 点击 工具 -> 设置 -> 插件配置 -> mxp-stylelint -> .stylelintrc.js
2. 在`.stylelintrc.js` 文件中把 `isOrder`设置为`false`

如在根目录有`.stylelintrc.js`文件的修改根目录的代码也行

```js
/** 是否使用stylelint-config-standard-scss格式化scss样式 */
const isScss = true;
/** 是否使用stylelint-order对css属性(如：width，height，padding，margin)进行排序 (启用后需要注意css中的条件编译) */
const isOrder = true;
```

## 使用 mxp 格式化全家桶

使用 mxp 格式化全家桶：[mxp-stylelint](https://ext.dcloud.net.cn/plugin?id=10860)、[mxp-prettier](https://ext.dcloud.net.cn/plugin?id=10850)、[mxp-eslint-vue](https://ext.dcloud.net.cn/plugin?id=10851)、[mxp-eslint-js](https://ext.dcloud.net.cn/plugin?id=10853) 可无缝切换至 vscode 开发，适合即用 vscode 又用 HBuilder 的用户。（只需要在 vscode 中 npm 安装对应插件并拷贝对应规则即可（mxp-eslint-vue 和 mxp-eslint-js 是的插件和规则是一样的直接使用 mxp-eslint-js 的规则就行了）），这样就不用担心 vscode 和 HBuilder 的格式化规则不一致的问题了

## 禁止自带的格式化工具 (js-beautify)

HBuilder 默认使用`js-beautify`格式化代码且不可卸载，当同时存在`js-beautify`和`prettier`插件时，`prettier`格式化`.vue`,`prettier`格式化,`js-beautify`格式化`.js`(详见：[HBuilderX 格式化操作、及格式化插件配置说明](https://ask.dcloud.net.cn/article/36529)),这样会导致 js 和 vue 不是用的同一个格式化工具，所以建议删除自带
`js-beautify`格式化工具，或者禁止使用`js-beautify`格式化，具体操作如下：

1. `HBuilder X`中点击 工具 -> 插件配置 -> jsbeautify -> 打开 jsbeautifyrc.js 进行配置。
2. 在配置文件中屏蔽 `parsers` 节点中的键值对，如图：

```js
module.exports = {
  options: {
    indent_size: '1',
    indent_char: '\t',
    // 由于篇幅太长下面的省略
  },
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  parsers: {
    // '.js': 'js',
    // '.json': 'js',
    // '.njs': 'js',
    // '.sjs': 'js',
    // '.wxs': 'js',
    // '.css': 'css',
    // '.nss': 'css',
    // '.wxss': 'css',
    // 由于篇幅太长下面的省略
  },
};
```

## 问题解答

### 明明下载的时最新版规则还是旧版的

由于用户的配置文件(工具 -> 设置 -> 插件配置 -> 打开对应的配置文件)会覆盖自带的配置文件，所以建议先备份后删除原配置文件以及他的上级目录，然后重新打开 工具 -> 设置 -> 插件配置 -> 对应的配置文件 ，此时会生成一个最新版的配置文件

### 修改项目根目录的配置文件不生效

本插件根目录的配置文件仅用于格式化所有文件时使用，在保存时验证请直接修改 工具 -> 设置 -> 插件配置 -> 中的配置文件

### 其他问题

其他问题请在[本插件评论区](https://ext.dcloud.net.cn/plugin?id=10851)或者提交[issues](https://gitee.com/mxp131011/mxp-format/issues)

## 感谢

本插件是基于[DCloud-HBuilderX](https://ext.dcloud.net.cn/publisher?id=37996) 团队制作的 [stylelint](https://ext.dcloud.net.cn/plugin?id=2044)插件的一个升级版,在此对原作者表示感谢
