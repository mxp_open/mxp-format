/** 是否使用stylelint-config-standard-scss格式化scss样式 */
const isScss = true;
/** 是否使用stylelint-order对css属性(如：width，height，padding，margin)进行排序 (启用后需要注意css中的条件编译) */
const isOrder = true;

module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-config-standard-scss', 'stylelint-config-html/vue'],
  overrides: [
    {
      customSyntax: 'postcss-scss',
      files: ['**/*.css', '**/*.scss'],
    },
    {
      customSyntax: 'postcss-less',
      files: ['**/*.less'],
    },
    {
      customSyntax: 'postcss-html',
      files: ['**/*.html', '**/*.vue', '**/*.nvue'],
    },
  ],
  plugins: isOrder ? ['stylelint-order'] : [],

  // 在某些文件或文件夹中禁用检查
  ignoreFiles: ['index.html', 'unpackage/**', 'node_modules/**', 'uni_modules/**'],
  // 规则
  rules: {
    ...baseRule(),
    ...(isScss ? scssRule() : {}),
    ...(isOrder ? orderRule() : {}),
  },
};

/**
 * 基础规则
 */
function baseRule() {
  return {
    'selector-anb-no-unmatchable': true, // 禁止不匹配的 An+B 选择器。(:nth-child(0n+0) 这里报错，结果匹配不到任何元素) （15.3.0新增）
    'color-no-invalid-hex': true, // 禁止使用无效的十六进制颜色
    'font-family-no-duplicate-names': true, // 禁止使用重复的字体名称。
    'font-family-no-missing-generic-family-keyword': [null, { ignoreFontFamilies: [] }], // 不允许在字体系列名称列表中缺少通用系列。
    'named-grid-areas-no-invalid': true, // 不允许无效的命名网格区域。
    'function-calc-no-unspaced-operator': true, // 禁止在 calc 函数中使用没有间隔的运算符。
    'function-linear-gradient-no-nonstandard-direction': true, // 禁止在 linear-gradient() 中调用不符合标准语法的无效方
    // 'function-no-unknown': true, // 禁止未知功能 在 "scss/function-no-unknown" 中开启
    'string-no-newline': true, // 禁止字符串中的(未转义)换行符。
    'unit-no-unknown': [true, { ignoreUnits: ['rpx'] }], // 禁止未知的单位。
    'custom-property-no-missing-var-function': true, // 自定义属性必须添加var()
    'property-no-unknown': [true, { checkPrefixed: true, ignoreSelectors: [':export', ':root'] }], // 禁止未知的属性
    'keyframe-declaration-no-important': true, // 在动画中禁止关键帧声明的 !important。
    'keyframe-block-no-duplicate-selectors': true, // 禁止关键帧块中的重复选择器。
    'declaration-block-no-duplicate-custom-properties': true, // 不允许重复自定义属性
    'declaration-block-no-duplicate-properties': [true, { ignoreProperties: ['image-rendering', 'position'] }], // 禁止声明块的重复属性。
    'declaration-block-no-shorthand-property-overrides': true, // 禁止简写属性覆盖相关的扩写属性。

    'block-no-empty': true, // 禁止空块。
    // 'comment-no-empty': null, // 禁止空注释。 在scss/comment-no-empty 中启用该规则
    'no-empty-source': true, // 禁止空源码。

    'selector-pseudo-class-no-unknown': [true, { ignorePseudoClasses: ['deep', 'export'] }], // 禁止未知的伪类选择器
    'selector-pseudo-element-no-unknown': true, // 禁止未知的伪元素选择器。
    'selector-type-no-unknown': [true, { ignore: ['custom-elements', 'default-namespace'] }], // 禁止未知的类型选择器。
    'media-feature-name-no-unknown': true, // 禁止未知的媒体功能名。

    'annotation-no-unknown': true, // 不允许使用未知批注
    'at-rule-no-unknown': null, // 不允许未知的规则。 // 在 scss/at-rule-no-unknown中开启
    // 'declaration-property-value-no-unknown': true, // 不允许声明中的属性出现未知值 😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍

    'no-descending-specificity': null, // 不允许优先级较低性的选择器出现在覆盖较高优先级的选择器之后。
    'no-duplicate-at-import-rules': true, // 禁止在样式表中使用重复的 @import 规则。
    'no-duplicate-selectors': true, // 禁止样式表中的重复选择器。

    'no-invalid-double-slash-comments': true, // 禁止 CSS 不支持并可能导致意外结果的双斜杠注释（//...）。
    'no-invalid-position-at-import-rule': true, // @import 只能在其他样式最前面
    'alpha-value-notation': 'number', // 指定alpha值的百分比或数字表示法（可自动修复）。
    'hue-degree-notation': 'angle', // 指定度数或角度表示法（可自动修复）。
    'color-function-notation': 'legacy', // 颜色代码同意使用逗号或空格分割
    'color-hex-alpha': 'never', // 要求或禁止十六进制颜色的 Alpha 通道。
    'color-hex-length': 'long', // 为十六进制颜色指定短表示法或长表示法,即是：#ffffff 还是#fff  "short" "long"
    'color-named': 'never', // 禁止颜色使用名称 如不能 color: black ，而应该使用color: #000000
    // 'color-no-hex': true, // 禁止十六进制颜色。
    'length-zero-no-unit': [true, { ignoreFunctions: ['var', '/^--/'] }], // 不允许使用零长度的单位。
    'font-family-name-quotes': 'always-unless-keyword', // 指定是否应在字体系列名称周围使用引号（可自动修复）
    'font-weight-notation': 'named-where-possible', //需要数字或命名（如果可能）font-weight值。此外，当需要命名值时，只需要有效的名称。
    // 'function-allowed-list': [],指定允许的功能列表。
    // 'function-disallowed-list': [],指定不允许使用的功能列表。
    'function-url-no-scheme-relative': true, // 禁止相对协议 URL。
    'function-url-quotes': 'always', // 要求或禁止 URL 的引号。（可自动修复）。
    // 'function-url-scheme-allowed-list': [], ///指定允许的 URL 方案列表。
    // 'function-url-scheme-disallowed-list': [], // 指定不允许的 URL 方案列表。
    'import-notation': 'string', // 使用@import url("xxxx.scss");  还是@import "xxxx.scss";
    'keyframe-selector-notation': 'percentage', // 禁止使用无效的十六进制颜色
    'media-feature-range-notation': 'prefix', // 指定媒体特征范围的上下文或前缀表示法
    'keyframes-name-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, //指定关键帧名称的模式(命名规则)
    'number-max-precision': 4, // 限制数字中允许的小数位数
    'time-min-milliseconds': 10, // 指定时间值的最小毫秒数
    // 'unit-allowed-list': ['rem', 'rpx'], //指定允许的单位列表。
    'unit-disallowed-list': ['rem', 'px', 'ms'], // 指定不允许的单位列表
    'shorthand-property-no-redundant-values': true, // 禁止简写属性的冗余值
    'value-no-vendor-prefix': [null, { ignoreValues: ['sticky', 'box'] }], // 值不允许出现供应商前缀（可自动修复）。
    'custom-property-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 指定自定义属性的命名规则
    // 'property-allowed-list': ['display', 'animation'], //指定允许的属性列表。
    'property-disallowed-list': ['background-image', 'background-color'], //指定不允许的属性列表。
    'property-no-vendor-prefix': [true, { ignoreProperties: ['mask-image', 'mask-size', 'user-select'] }], // 属性不允许出现供应商前缀（可自动修复）。
    // 'declaration-no-important': true, // 禁止使用 !important
    // 'declaration-property-max-values': { border: 2 }, // 限制属性的最大值
    // 'declaration-property-unit-allowed-list': { '/^animation/': 's' }, // 限制属性的值的单位
    // 'declaration-property-unit-disallowed-list': { '/^animation/': 'ms' }, //指定声明中不允许的属性和单位对的列表
    // 'declaration-property-value-allowed-list': { transform: ['/scale/'] }, // 指定声明中允许的属性和值对的列表
    // 'declaration-property-value-disallowed-list': { transform: ['/scale/'] }, // 指定声明中禁止的属性和值对的列表
    'declaration-block-no-redundant-longhand-properties': true, // 禁止冗余的写法
    'declaration-block-single-line-max-declarations': 1, // 一行中最大允许的属性数量
    'selector-attribute-name-disallowed-list': ['id', '/^data-/'], // 指定不允许的属性名称的列表
    'selector-attribute-operator-allowed-list': ['=', '*=', '^='], // 指定允许的属性运算符的列表
    // 'selector-attribute-operator-disallowed-list': ['=', '*=', '^='], // 指定不允许的属性运算符的列表
    'selector-attribute-quotes': 'always', // 要求在块的开大括号之前必须有一个空格或不能有空白符。
    'selector-class-pattern': /(^[a-z][a-z0-9]*(-[a-z0-9]+)*$)|(^el-[a-z-_]+[a-z]$)/, // 【 特殊 】 指定类选择器的模式(即class的命名规范)
    'selector-combinator-allowed-list': ['+', '>', ' '], //指定允许的组合器的列表
    // 'selector-combinator-disallowed-list': ['+', '>', ' '], // 指定不允许的组合器的列表
    // 'selector-disallowed-list': ['.foo', '.foo .aa'], // 指定不允许的选择器列表
    'selector-id-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 指定ID选择器的模式(即id的命名规范)
    'selector-max-attribute': 2, //限制选择器中的属性选择器的数量
    // 'selector-max-class': 10, // 限制选择器中的类的最大数量
    // 'selector-max-combinators': 10, // 限制选择器中组合器的最大数量 （组合器即： > +  ~ 空格 等等）
    // 'selector-max-compound-selectors': 10, // 所有选择器的最大数量 （包含id class 等选择器）
    'selector-max-id': 0, //限制选择器中 ID 选择器的最大数量
    // 'selector-max-pseudo-class': 3, // 限制选择器中伪类的数量
    // 'selector-max-specificity': '0,2,0', // 限制选择器的特异性
    // 'selector-max-type': 2, // 限制选择器中类型选择器的数量
    'selector-max-universal': 0, // 限制选择器中通用选择器的数量
    // 'selector-nested-pattern': '^&:(?:hover|focus)$', // 为嵌套在规则中的规则选择器指定规则
    'selector-no-qualifying-type': [true, { ignore: ['class', 'attribute'] }], // 不允许按类型限定选择器
    'selector-no-vendor-prefix': true, // 不允许使用选择器的供应商前缀
    'selector-not-notation': 'simple', // 为伪类指定简单或复杂表示法。:not()
    // 'selector-pseudo-class-allowed-list': ['hover', '/^nth-/'], // 指定允许的伪类选择器的列表
    // 'selector-pseudo-class-disallowed-list': ['hover'], // 指定不允许的伪类选择器的列表
    // 'selector-pseudo-element-allowed-list': ['before', 'after'], // 指定允许的伪元素选择器的列表
    'selector-pseudo-element-colon-notation': 'single', // 指定伪元素适用单冒号还是双冒号表示法（可自动修复）。
    'selector-pseudo-element-disallowed-list': ['before', 'after'], // 指定不允许的伪元素选择器的列表
    // 'rule-selector-property-disallowed-list': { div: ['color', '/margin/'] }, // 指定每个元素不允许的属性
    // 'media-feature-name-allowed-list': ['min-width'], // 指定允许的媒体功能名称列表
    // 'media-feature-name-disallowed-list': ['min-width'], // 指定不允许的媒体功能名称列表
    'media-feature-name-no-vendor-prefix': true, // 不允许使用媒体功能名称的供应商前缀
    // 'media-feature-name-unit-allowed-list': [true, { 'width': 'em', '/height/': ['em', 'rem'] }], // 指定媒体功能中允许的名称和设备对的列表
    // 'media-feature-name-value-allowed-list': { 'min-width': ['768px', '1024px'] }, // 指定允许的媒体功能名称和值对的列表
    'custom-media-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 指定自定义媒体查询名称的模式 (命名规则)
    // 'at-rule-allowed-list': ['extend', 'keyframes'], // 指定允许的规则列表
    'at-rule-disallowed-list': ['extend'], //指定不允许的规则列表
    'at-rule-no-vendor-prefix': true, // 不允许使用供应商前缀作为 at 规则
    // 'at-rule-property-required-list': { 'font-face': ['font-display', 'font-family', 'font-style'] }, // 指定 at 规则的必需属性列表
    // 'comment-pattern': 'foo .+', // 指定注释的规则
    // 'comment-word-disallowed-list': [true, '/^TODO:/', 'badword'], // 指定注释中不允许的单词列表
    // 'max-nesting-depth': 2, // 限制嵌套的深度
    'no-irregular-whitespace': true, // 禁止使用不规则空格
    'no-unknown-animations': true, // 禁止未知的动画。
    'value-keyword-case': 'lower', // 指定关键字值的大小写（可自动修复）。
    'function-name-case': 'lower', // 指定函数名的大小写（可自动修复）。
    'custom-property-empty-line-before': 'never', // 要求或禁止在自定义属性之前使用空行（可自动修复）。
    'declaration-empty-line-before': ['never', { ignore: 'after-comment' }], // 在声明之前要求或禁止空行。
    'selector-type-case': 'lower', // 指定类型选择器的大小写（可自动修复）。
    'rule-empty-line-before': ['always', { ignore: ['after-comment', 'first-nested'] }], // 要求或禁止在规则之前的空行（可自动修复）。
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-blockless', 'first-nested', 'blockless-after-same-name-blockless', 'after-same-name'],
        ignore: ['after-comment'],
      },
    ], // 要求或禁止在 at 规则之前使用空行
    'comment-empty-line-before': ['always', { except: ['first-nested'], ignoreComments: ['/^#/'] }], // 要求或禁止在注释之前的空行（可自动修复）。
    'comment-whitespace-inside': 'always', // 要求或禁止注释标记内侧的空白符（可自动修复）。

    /* ****************************************** 15.0.0中以废弃的规则 ****************************************** */
    // 'color-hex-case': 'lower', // 颜色使用小写 👇
    // 'function-comma-newline-after': 'always-multi-line', // 在函数的逗号后需要换行符或不允许使用空格（可自动修复）。 👇
    // 'function-comma-newline-before': 'always-multi-line', // 在函数的逗号前需要换行符或不允许使用空格（可自动修复）。👇
    // 'function-comma-space-after': 'always', // 要求在函数的逗号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'function-comma-space-before': 'never', // 要求在函数的逗号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'function-max-empty-lines': 0, // 限制函数中相邻空行的数量（可自动修复）👇
    // 'function-parentheses-newline-inside': 'always-multi-line', // 函数括号内侧需要换行符或不允许在使用空格（可自动修复）👇
    // 'function-parentheses-space-inside': 'never', // 要求在函数的括号内侧必须有一个空格或不能有空白符（可自动修复）。👇
    // 'function-whitespace-after': 'always', // 要求或禁止函数之后的空白符（可自动修复）。👇
    // 'number-leading-zero': 'always', // 要求或禁止小于 1 的小数有一个前导零（可自动修复）。👇
    // 'number-no-trailing-zeros': true, // 禁止数量的尾随零（可自动修复）。👇
    // 'string-quotes': 'single', // 指定字符串使用单引号或双引号（可自动修复）。👇
    // 'unit-case': 'lower', // 指定单位的大小写（可自动修复）。👇
    // 'value-list-comma-newline-after': 'always-multi-line', // 在多行值列表中，逗号后必须始终有一个换行符。👇
    // 'value-list-comma-newline-before': 'never-multi-line', // 多行值列表中的逗号前不得有空格。👇
    // 'value-list-comma-space-after': 'always-single-line', // 要求在值列表的逗号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'value-list-comma-space-before': 'never', // 要求在值列表的逗号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'value-list-max-empty-lines': 0, // 限制值列表中相邻空行的数量（可自动修复）👇
    // 'property-case': ['lower', { ignoreSelectors: [':export'] }], // 指定属性的大小写（可自动修复）。👇
    // 'declaration-bang-space-after': 'never', // 要求在声明的叹号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'declaration-bang-space-before': 'always', // 要求在声明的叹号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'declaration-colon-newline-after': 'always-multi-line', // 要求在声明块的冒号之后必须有换行符或不能有空白符（可自动修复）。👇
    // 'declaration-colon-space-after': 'always', // 要求在声明块的冒号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'declaration-colon-space-before': 'never', // 要求在声明块的冒号之前必须有一个空格或不能有空白符（可自动修复）。
    // 'declaration-block-semicolon-newline-after': 'always', // 要求在声明块的分号之后必须有换行符或不能有空格（可自动修复）。👇
    // 'declaration-block-semicolon-newline-before': 'never-multi-line', //要求在声明块的分号之前必须有换行符或不能有空格（可自动修复）。👇
    // 'declaration-block-semicolon-space-after': 'always-single-line', // 要求在声明块的分号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'declaration-block-semicolon-space-before': 'never', // 要求在声明块的分号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'declaration-block-trailing-semicolon': null, // 要求或禁止声明块的一个尾随分号（可自动修复）。（与prettier有冲突）👇
    // 'block-closing-brace-empty-line-before': 'never', // 要求或禁止在块的闭大括号之前空行（可自动修复）。👇
    // 'block-closing-brace-newline-after': 'always', // 要求在块的闭大括号之后必须有换行符或不能有空白符（可自动修复）。👇
    // 'block-closing-brace-newline-before': 'always', // 要求在块的闭大括号之前必须有换行符或不能有空白符（可自动修复）。👇
    // 'block-closing-brace-space-after': 'always-single-line', // 需要单个空格或不允许在块的右大括号后使用空格（可自动修复）。👇
    // 'block-closing-brace-space-before': 'always-single-line', // 在块的右大括号之前需要一个空格或不允许空格（可自动修复）。👇
    // 'block-opening-brace-newline-after': 'always', // 要求在块的开大括号之后必须有换行符（可自动修复）。👇
    // 'block-opening-brace-newline-before': 'never-single-line', // 要求换行符或不允许在块的左大括号前使用空格（可自动修复）。👇
    // 'block-opening-brace-space-after': 'always-single-line', // 需要单个空格或不允许在块的左大括号后使用空格。（可自动修复）。👇
    // 'block-opening-brace-space-before': 'always', // 需要单个空格或不允许在块的左大括号之前使用空格（可自动修复）。👇
    // 'selector-attribute-brackets-space-inside': 'never', // 要求在属性选择器的中括号内侧必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-attribute-operator-space-after': 'never', // 要求在属性选择器中的运算符之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-attribute-operator-space-before': 'never', // 要求在属性选择器中的运算符之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-combinator-space-after': 'always', // 要求在组合选择器之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-combinator-space-before': 'always', // 要求在组合选择器之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-descendant-combinator-no-non-space': true, // 禁止后代选择器使用非空格字符（可自动修复）。👇
    // 'selector-max-empty-lines': 0, // 限制选择器中相邻空行的数量 （可自动修复）。👇
    // 'selector-pseudo-class-case': 'lower', // 指定伪类选择器的大小写（可自动修复）。👇
    // 'selector-pseudo-class-parentheses-space-inside': 'never', // 要求在伪类选择器的括号内侧必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-pseudo-element-case': 'lower', // 指定伪元素选择器的大小写。👇
    // 'selector-list-comma-newline-after': 'always', // 要求在选择器列表的逗号之后必须有换行符或不能有空白符（可自动修复）。👇
    // 'selector-list-comma-newline-before': 'never-multi-line', // 要求在选择器列表的逗号之前必须有换行符或不能有空白符（可自动修复）。👇
    // 'selector-list-comma-space-after': 'always-single-line', // 要求在选择器列表的逗号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'selector-list-comma-space-before': 'never', // 要求在选择器列表的逗号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-feature-colon-space-after': 'always', // 要求在媒体功能的冒号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-feature-colon-space-before': 'never', // 要求在媒体功能的冒号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-feature-name-case': 'lower', // 指定媒体功能名的大小写（可自动修复）。👇
    // 'media-feature-parentheses-space-inside': 'never', // 要求在媒体功能的括号内侧必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-feature-range-operator-space-after': 'always', // 要求在媒体功能的范围运算符之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-feature-range-operator-space-before': 'always', // 要求在媒体功能的范围运算符之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-query-list-comma-newline-after': 'always-multi-line', // 需要换行符或不允许在媒体查询列表的逗号后使用空格。👇
    // 'media-query-list-comma-newline-before': 'never-multi-line', // 需要单个空格或不允许在媒体查询列表的逗号前使用空格。👇
    // 'media-query-list-comma-space-after': 'always-single-line', // 要求在媒体查询列表的逗号之后必须有一个空格或不能有空白符（可自动修复）。👇
    // 'media-query-list-comma-space-before': 'never', // 要求在媒体查询列表的逗号之前必须有一个空格或不能有空白符（可自动修复）。👇
    // 'at-rule-name-case': 'lower', // 指定@规则名的大小写（可自动修复）。👇
    // 'at-rule-name-newline-after': 'always-multi-line', // 要求在规则名称后使用换行符（可自动修复）。👇
    // 'at-rule-name-space-after': 'always', // 要求在@规则名之后必须有一个空格（可自动修复）。👇
    // 'at-rule-semicolon-newline-after': 'always', // 要求在@规则的分号之后必须有换行符（可自动修复）。👇
    // 'at-rule-semicolon-space-before': 'never', // 要求在@规则的分号之前必须有一个空格或不能有空白符。（可自动修复）。👇
    // 'indentation': 2, // 指定缩进（可自动修复）。 👇
    // 'linebreaks': 'unix', // 指定换行符（可自动修复）。👇
    // 'max-empty-lines': 2, // 限制相邻空行的数量。👇
    // 'max-line-length': 160, // 一行的最大长度 👇
    // 'no-empty-first-line': true, // 文件开头禁止换行（可自动修复）。👇
    // 'no-eol-whitespace': true, // 不允许使用行尾空格 👇
    // 'no-extra-semicolons': true, // 禁止额外的分号（可自动修复）。👇
    // 'no-missing-end-of-source-newline': true, // 文件末尾必须换行（可自动修复）。👇
    // 'unicode-bom': 'never', //要求或禁止 Unicode 字节顺序标记 👇
  };
}

/**
 * scss规则
 */
function scssRule() {
  return {
    'scss/at-each-key-value-single-line': true, // 用于检查用户使用 map-keys 或 map.keys 执行循环并在循环内获取该键的值的情况
    // 'scss/at-else-closing-brace-newline-after': 'always-last-in-chain', // 在语句的右大括号之后要求或不允许换行符（可自动修复） [与 block-closing-brace-newline-after可能有冲突]
    'scss/at-else-closing-brace-space-after': 'always-intermediate', // 需要单个空格或不允许在语句的右大括号后使用空格（自动修复）。@else
    'scss/at-else-empty-line-before': 'never', // 要求在 -else 之前使用空行或不允许空行（可自动修复）。@else
    'scss/at-else-if-parentheses-space-before': 'always', // 要求或不允许在括号前加空格（可自动修复）。@else if
    'scss/at-extend-no-missing-placeholder': true, // 禁止缺少占位符的 @extend
    'scss/at-function-named-arguments': 'never', // 需要或禁止在 SCSS 函数调用规则中指定参数名称 animation: animation($duration: 250ms) 或 animation: animation(250ms)
    'scss/at-function-parentheses-space-before': 'never', // 要求或不允许在括号前加空格
    'scss/at-function-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 为类似 Sass/SCSS 的函数名称指定命名规则
    // 'scss/at-if-closing-brace-newline-after': 'always-last-in-chain', // 要求或禁止在语句的右大括号后使用换行符 [与 block-closing-brace-newline-after 有冲突]
    'scss/at-if-closing-brace-space-after': 'always-intermediate', // 需要单个空格或不允许在语句的右大括号后使用空格
    // 'scss/at-if-no-null': true, // 检查是否等于 null 是不必要的显式，因为在 Sass 中是 falsey 即 @if $x == null 可改写为 @if not $x
    'scss/at-import-no-partial-leading-underscore': true, // 禁止在@import路径中使用前缀下划线
    'scss/at-import-partial-extension': 'always', // 要求或禁止命令中的扩展
    // 'scss/at-import-partial-extension-blacklist': ['lass', 'less'], // 禁止@import扩展的黑名单
    'scss/at-import-partial-extension-whitelist': [/scss/], // 允许@import扩展名的白名单
    'scss/at-mixin-argumentless-call-parentheses': 'always', // 在无参数调用中要求或禁止使用括号
    'scss/at-mixin-named-arguments': 'never', // 在 @include() 调用规则中需要命名参数或不允许使用参数 @include aa($bbb: 250ms) 或者 @include aa(250ms)
    'scss/at-mixin-parentheses-space-before': 'never', //要求或不允许在括号前加空格
    'scss/at-mixin-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 允许@import扩展名的白名单
    //  'scss/at-rule-conditional-no-parentheses': true, // 不允许在@条件规则中使用括号（@if， @elsif， @while）
    'scss/at-rule-no-unknown': true, //要求或不允许在括号前加空格
    // 'scss/at-use-no-unnamespaced': true, // 不允许使用没有命名空间的 @use

    'scss/dollar-variable-colon-newline-after': 'always-multi-line', // 在 $变量声明中，冒号后需要换行符
    'scss/dollar-variable-colon-space-after': 'always-single-line', // 在$变量声明中，需要或不允许在冒号后使用空格
    'scss/dollar-variable-colon-space-before': 'never', // 在$变量声明中，需要或不允许在冒号前使用空格
    // 'scss/dollar-variable-default': [true, { ignore: 'local' }], // $变量声明的需要标志!default
    // 需要空行或不允许在$变量声明后使用空行
    'scss/dollar-variable-empty-line-after': [
      'always',
      {
        except: ['last-nested', 'before-comment', 'before-dollar-variable'],
        ignore: ['before-comment', 'inside-single-line-block'],
      },
    ],
    // 需要空行或不允许在$变量声明前使用空行
    'scss/dollar-variable-empty-line-before': [
      'always',
      {
        except: ['first-nested', 'after-comment', 'after-dollar-variable'],
        ignore: ['after-comment', 'inside-single-line-block', 'after-dollar-variable'],
      },
    ],
    'scss/dollar-variable-first-in-block': [true, { ignore: ['comments', 'imports'] }], //要求将 -变量声明放在块（根或规则）的第一个位置
    'scss/dollar-variable-no-missing-interpolation': true, // 禁止使用未经插值的 Sass 变量与使用自定义标识符的 CSS 功能
    'scss/dollar-variable-no-namespaced-assignment': true, // 不允许分配到命名空间变量
    'scss/dollar-variable-pattern': /^[_a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 为变量名称指定命名规则
    'scss/percent-placeholder-pattern': /^[a-z][a-z0-9]*(-[a-z0-9]+)*$/, // 为%占位符名称指定命名规则
    //要求或禁止在注释之前使用空行
    'scss/double-slash-comment-empty-line-before': [
      'always',
      {
        except: ['first-nested', 'inside-block'],
        ignore: ['between-comments', 'stylelint-commands', 'inside-block'],
      },
    ],
    // 'scss/double-slash-comment-inline': 'always', // 要求或不允许 -注释为内联注释
    'scss/double-slash-comment-whitespace-inside': 'always', // 要求或不允许在注释后面留空格
    'scss/comment-no-empty': true, // 为变量名称指定命名规则
    'scss/comment-no-loud': null, // 禁止/** **/注释,而全部使用 双斜杠注释
    'scss/declaration-nested-properties': 'never', // 要求或禁止名称中包含的属性采用嵌套组的形式
    'scss/declaration-nested-properties-no-divided-groups': true, // 不允许将同一“命名空间”的嵌套属性划分为多个组
    'scss/dimension-no-non-numeric-values': true, // 用单位（例如 ）插入值会产生字符串值，而不是数值。然后，此值不能在数值运算中使用。最好使用算术将单位应用于数字
    // 'scss/function-color-relative': true, // 鼓励使用刻度颜色
    'scss/function-no-unknown': [true, { ignoreFunctions: ['constant'] }], // 禁止未知函数
    // 'scss/function-quote-no-quoted-strings-inside': true, // 不允许在引号函数中使用带引号的字符串
    'scss/function-unquote-no-unquoted-strings-inside': true, // 不允许在 unquote 函数中使用未加引号的字符串
    // 'scss/map-keys-quotes': 'always', // 要求在Sass中key必须加引号
    // 'scss/media-feature-value-dollar-variable': 'always', // 要求媒体功能值为 -变量或不允许媒体功能值中的 -变量
    'scss/operator-no-newline-after': true, // 禁止在 Sass 运算符之后换行
    'scss/operator-no-newline-before': true, // 禁止在 Sass 运算符之前换行
    'scss/operator-no-unspaced': true, // 禁止在 Sass 操作中使用无间隔运算符
    'scss/partial-no-import': true, // 不允许在部分文件中使用非 .CSS导入
    // 'scss/selector-nest-combinators': true, // 要求或禁止在选择器中嵌套组合器
    'scss/selector-no-redundant-nesting-selector': true, // 禁止冗余嵌套选择器
    'scss/selector-no-union-class-name': true, // 不允许将类名与父选择器合并
    // 'scss/no-dollar-variables': true, // 不允许在样式表中使用美元变量
    'scss/no-duplicate-dollar-variables': true, // 不允许在样式表中使用重复的美元变量
    'scss/no-duplicate-mixins': true, // 禁止在样式表中使用重复的mixin
    'scss/no-global-function-names': null, // 不允许使用全局函数名称，因为这些全局函数现在位于内置 Sass 模块中
  };
}

/**
 * order规则
 */
function orderRule() {
  return {
    'order/order': [
      // 排序
      'dollar-variables',
      'custom-properties',
      'at-rules',
      'declarations',
      { type: 'at-rule', name: 'supports' },
      { type: 'at-rule', name: 'media' },
      'rules',
    ],

    'order/properties-order': [
      { properties: ['all'] },
      {
        properties: ['position', 'inset', 'inset-block', 'inset-inline', 'top', 'right', 'bottom', 'left', 'z-index'],
      },
      { properties: ['box-sizing', 'display'] },
      {
        properties: ['flex', 'flex-basis', 'flex-direction', 'flex-flow', 'flex-grow', 'flex-shrink', 'flex-wrap'],
      },
      {
        properties: [
          'grid',
          'grid-area',
          'grid-template',
          'grid-template-areas',
          'grid-template-rows',
          'grid-template-columns',
          'grid-row',
          'grid-row-start',
          'grid-row-end',
          'grid-column',
          'grid-column-start',
          'grid-column-end',
          'grid-auto-rows',
          'grid-auto-columns',
          'grid-auto-flow',
          'grid-gap',
          'grid-row-gap',
          'grid-column-gap',
        ],
      },
      { properties: ['gap', 'row-gap', 'column-gap'] },
      {
        properties: [
          'place-content',
          'place-items',
          'place-self',
          'align-content',
          'align-items',
          'align-self',
          'justify-content',
          'justify-items',
          'justify-self',
        ],
      },
      { properties: ['order'] },
      {
        properties: [
          'float',
          'width',
          'min-width',
          'max-width',
          'height',
          'min-height',
          'max-height',
          'padding',
          'padding-top',
          'padding-right',
          'padding-bottom',
          'padding-left',
          'margin',
          'margin-top',
          'margin-right',
          'margin-bottom',
          'margin-left',
          'overflow',
          'overflow-x',
          'overflow-y',
          '-webkit-overflow-scrolling',
          '-ms-overflow-x',
          '-ms-overflow-y',
          '-ms-overflow-style',
          'overscroll-behavior',
          'overscroll-behavior-x',
          'overscroll-behavior-y',
          'overscroll-behavior-inline',
          'overscroll-behavior-block',
          'clip',
          'clip-path',
          'clear',
        ],
      },
      {
        properties: [
          'font',
          'font-family',
          'font-size',
          'font-style',
          'font-weight',
          'font-feature-settings',
          'font-kerning',
          'font-variant',
          'font-variant-ligatures',
          'font-variant-caps',
          'font-variant-alternates',
          'font-variant-numeric',
          'font-variant-east-asian',
          'font-variant-position',
          'font-size-adjust',
          'font-stretch',
          'font-effect',
          'font-emphasize',
          'font-emphasize-position',
          'font-emphasize-style',
          '-webkit-font-smoothing',
          '-moz-osx-font-smoothing',
          'font-smooth',
          'hyphens',
          'line-height',
          'color',
          'text-align',
          'text-align-last',
          'text-emphasis',
          'text-emphasis-color',
          'text-emphasis-style',
          'text-emphasis-position',
          'text-decoration',
          'text-decoration-line',
          'text-decoration-thickness',
          'text-decoration-style',
          'text-decoration-color',
          'text-underline-position',
          'text-underline-offset',
          'text-indent',
          'text-justify',
          'text-outline',
          '-ms-text-overflow',
          'text-overflow',
          'text-overflow-ellipsis',
          'text-overflow-mode',
          'text-shadow',
          'text-transform',
          'text-wrap',
          '-webkit-text-size-adjust',
          '-ms-text-size-adjust',
          'letter-spacing',
          'word-break',
          'word-spacing',
          'word-wrap', // Legacy name for `overflow-wrap`
          'overflow-wrap',
          'tab-size',
          'white-space',
          'vertical-align',
          'list-style',
          'list-style-position',
          'list-style-type',
          'list-style-image',
        ],
      },
      {
        properties: [
          'pointer-events',
          '-ms-touch-action',
          'touch-action',
          'cursor',
          'visibility',
          'zoom',
          'table-layout',
          'empty-cells',
          'caption-side',
          'border-spacing',
          'border-collapse',
          'content',
          'quotes',
          'counter-reset',
          'counter-increment',
          'resize',
          'user-select',
          'nav-index',
          'nav-up',
          'nav-right',
          'nav-down',
          'nav-left',
        ],
      },
      {
        properties: [
          'background',
          'background-color',
          'background-image',
          "-ms-filter:\\'progid:DXImageTransform.Microsoft.gradient",
          'filter:progid:DXImageTransform.Microsoft.gradient',
          'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader',
          'filter',
          'background-repeat',
          'background-attachment',
          'background-position',
          'background-position-x',
          'background-position-y',
          'background-clip',
          'background-origin',
          'background-size',
          'background-blend-mode',
          'isolation',
          'border',
          'border-color',
          'border-style',
          'border-width',
          'border-top',
          'border-top-color',
          'border-top-style',
          'border-top-width',
          'border-right',
          'border-right-color',
          'border-right-style',
          'border-right-width',
          'border-bottom',
          'border-bottom-color',
          'border-bottom-style',
          'border-bottom-width',
          'border-left',
          'border-left-color',
          'border-left-style',
          'border-left-width',
          'border-radius',
          'border-top-left-radius',
          'border-top-right-radius',
          'border-bottom-right-radius',
          'border-bottom-left-radius',
          'border-image',
          'border-image-source',
          'border-image-slice',
          'border-image-width',
          'border-image-outset',
          'border-image-repeat',
          'outline',
          'outline-width',
          'outline-style',
          'outline-color',
          'outline-offset',
          'box-shadow',
          'mix-blend-mode',
          'filter:progid:DXImageTransform.Microsoft.Alpha(Opacity',
          "-ms-filter:\\'progid:DXImageTransform.Microsoft.Alpha",
          'opacity',
          '-ms-interpolation-mode',
        ],
      },
      {
        properties: [
          'alignment-baseline',
          'baseline-shift',
          'dominant-baseline',
          'text-anchor',
          'word-spacing',
          'writing-mode',

          'fill',
          'fill-opacity',
          'fill-rule',
          'stroke',
          'stroke-dasharray',
          'stroke-dashoffset',
          'stroke-linecap',
          'stroke-linejoin',
          'stroke-miterlimit',
          'stroke-opacity',
          'stroke-width',

          'color-interpolation',
          'color-interpolation-filters',
          'color-profile',
          'color-rendering',
          'flood-color',
          'flood-opacity',
          'image-rendering',
          'lighting-color',
          'marker-start',
          'marker-mid',
          'marker-end',
          'mask',
          'shape-rendering',
          'stop-color',
          'stop-opacity',
        ],
      },
      {
        properties: [
          'transition',
          'transition-delay',
          'transition-timing-function',
          'transition-duration',
          'transition-property',
          'transform',
          'transform-origin',
          'animation',
          'animation-name',
          'animation-duration',
          'animation-play-state',
          'animation-timing-function',
          'animation-delay',
          'animation-iteration-count',
          'animation-direction',
        ],
      },
    ],
  };
}
