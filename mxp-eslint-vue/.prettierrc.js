module.exports = {
  // 一行最多可以有多少个字符
  printWidth: 160,
  // 一个tab相当于多少个空格
  tabWidth: 4,
  // 是否使用使用tab缩进
  useTabs: false,
  // 是否在每个语句的末尾添加分号
  semi: true,
  // 使用单引号而不是双引号。
  singleQuote: true,
  // 在JSX中使用单引号而不是双引号
  jsxSingleQuote: false,
  // 对象的key是否用引号括起来  "as-needed" - 仅在需要时在对象属性周围添加引号   "consistent" - 如果对象中的至少一个属性需要加引号，就对所有属性加引号   "preserve" - 按照对象属性中引号的输入用法
  quoteProps: 'consistent',
  // 若为true，文件顶部加了 /** @prettier */或/** @format */的文件才会被格式化
  requirePragma: false,
  // Prettier可以在文件的顶部插入一个 @format的特殊注释，以表明改文件已经被Prettier格式化过了。在使用 --require-pragma参数处理一连串的文件时这个功能将十分有用。如果文件顶部已经有一个doclock，这个选项将新建一行注释，并打上@format标记。
  insertPragma: false,
  // 超过最大宽度是否换行 "always" - 如果超过最大宽度，请换行。 "never" - 不要换行。  "preserve" - 按原样显示。首先在v1.9.0中提供
  proseWrap: 'preserve',
  // 在单个箭头函数参数周围加上括号    avoid" - 尽可能省略parens。例：x => x  "always" - 始终包括parens。例：(x) => x
  arrowParens: 'always',
  // 在对象，数组括号与文字之间加空格 "{ foo: bar }"
  bracketSpacing: true,
  // 置统一的行结尾换行符  "lf" – 仅换行(\n),在Linux和macOS以及git repos内部通用 "crlf" - 回车符+换行符(\r\n)在Windows上很常见  "cr" - 仅回车符(\r)，很少使用   "auto" - 保持现有的行尾（通过查看第一行后的内容对一个文件中的混合值进行归一化）
  endOfLine: 'auto',
  // 是否显示HTML文件中的空格。 有效选项： 'css' -遵循 CSS 属性的默认值。 'strict' - 所有标签周围的空格（或缺少空格）被认为是重要的。 'ignore' - 所有标签周围的空格（或缺少空格）被认为是微不足道的
  htmlWhitespaceSensitivity: 'ignore',
  // 如果为true则把'>'放在末尾而不是单独放一行
  bracketSameLine: false,
  // 是否格式化嵌入在文件中的带引号的代码
  embeddedLanguageFormatting: 'auto',
  // 在 HTML、Vue 和 JSX 中每行强制实施单个属性
  singleAttributePerLine: false,
  // 在数组对象等末尾添加逗号  "none" - 没有尾随逗号。 "es5" - 在ES5中有效的尾随逗号（对象，数组等）  "all" - 尽可能使用尾随逗号（包括函数参数）。这需要 nodejs 8
  trailingComma: 'es5',
  // 是否给vue中的 <script>和<style>标签加缩进
  vueIndentScriptAndStyle: true,
};
