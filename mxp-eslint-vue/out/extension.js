const hx = require('hbuilderx');
const WorkspaceEdit = hx.WorkspaceEdit;
const TextEdit = hx.TextEdit;
const eslint = require('eslint');
const fs = require('fs');
const diff = require('./diff.js');
let baseConfig = require('../.eslintrc.js');
const pluginName = require('../package.json').name;
const nls = require('hxnls');
const localize = nls.loadMessageBundle(__filename);
const path = require('path');
let extensionDir = undefined;
function resolveSettings(doc, autofix) {
    const userConfigFile = `${hx.env.appData}/extensions/mxp-eslint-vue/.eslintrc.js`;

    if (fs.existsSync(userConfigFile)) {
        delete require.cache[require.resolve(userConfigFile)];
        baseConfig = require(userConfigFile);
    }
    // TODO 这里应该用workspace.getWorkspaceFolder API
    const workspaceFolder = doc.workspaceFolder;
    // let CLIEngine = eslint.CLIEngine;
    // console.log(eslint)
    let CLIEngine = eslint.ESLint;
    let cwd = extensionDir || process.cwd();
    let ignorePath = null;
    let resolvePluginsRelativeTo = extensionDir;
    if (workspaceFolder && workspaceFolder.uri) {
        const folderUri = workspaceFolder.uri;
        const folderPath = typeof folderUri === 'string' ? folderUri : folderUri.fsPath;
        const userEslint = `${folderPath}/node_modules/eslint`;
        if (fs.existsSync(userEslint)) {
            const userEslintR = require(userEslint);
            // CLIEngine = userEslintR.CLIEngine;
            CLIEngine = userEslintR.ESLint;
            if (CLIEngine === undefined) {
                // let text = localize("resolveSettings.hx.window.showErrorMessage", "当前项目下使用的eslint版本过低，请使用不低于1.0.0版本的eslint。");
                // hx.window.showErrorMessage(text);
                if (userEslintR.CLIEngine) {
                    const isEnableVueplugin = fs.existsSync(`${userEslint}-plugin-vue`);
                    if (isEnableVueplugin) {
                        cwd = folderPath;
                        resolvePluginsRelativeTo = undefined;
                        CLIEngine = userEslintR.CLIEngine;
                        const userIgnore = `${folderPath}/.eslintignore`;
                        if (fs.existsSync(userIgnore)) {
                            ignorePath = userIgnore;
                        }
                    }
                } else {
                    CLIEngine = eslint.ESLint;
                }
            } else {
                const isEnableVueplugin = fs.existsSync(`${userEslint}-plugin-vue`);
                if (isEnableVueplugin) {
                    cwd = folderPath;
                    resolvePluginsRelativeTo = folderPath;
                    const userIgnore = `${folderPath}/.eslintignore`;
                    if (fs.existsSync(userIgnore)) {
                        ignorePath = userIgnore;
                    }
                }
            }
        }
    }

    const result = {
        version: CLIEngine.version,
        library: {
            CLIEngine,
        },
        workingDirectory: cwd,
        options: {
            baseConfig,
            fix: autofix,
            ignorePath,
            resolvePluginsRelativeTo,
        },
    };
    return result;
}
function withCLIEngine(callback, settings) {
    const cwd = process.cwd();
    const newOptions = settings.options;
    try {
        if (settings.workingDirectory) {
            newOptions.cwd = settings.workingDirectory;
        }
        const cli = new settings.library.CLIEngine(newOptions);
        return callback(cli);
    } finally {
        if (cwd !== process.cwd()) {
            process.chdir(cwd);
        }
    }
}
function validateAndFixDocument(doc, autofix) {
    if (doc.languageId !== 'vue') {
        return;
    }
    const text1 = localize(0, null, pluginName);
    hx.window.setStatusBarMessage(text1, 5 * 1000);
    const text = doc.getText();
    const settings = resolveSettings(doc, autofix);

    withCLIEngine(async (cli) => {
        let report = null;
        const curVersion = settings.version && settings.version.split('.')[0];
        let isPathIgnore = '';
        try {
            if (curVersion < 7) {
                isPathIgnore = await cli.isPathIgnored(doc.fileName);
                if (!isPathIgnore) {
                    report = cli.executeOnText(text, doc.fileName, true);
                }
            } else {
                isPathIgnore = await cli.isPathIgnored(doc.fileName);
                const options = {
                    filePath: doc.fileName,
                    warnIgnored: true,
                };
                report = {};
                if (!isPathIgnore) {
                    report.results = await cli.lintText(text, options);
                }
            }
        } catch (err) {
            printError(err);
            return;
        }
        const errorMsgs = [];
        if (report && report.results && Array.isArray(report.results) && report.results.length === 1) {
            const result = report.results[0];
            const messages = result.messages;
            if (messages && messages.length > 0) {
                for (let j = 0; j < messages.length; j++) {
                    const errorMsg = messages[j];
                    if (Number(errorMsg.severity) === 2) {
                        errorMsgs.push({
                            column: errorMsg.column - 1,
                            line: errorMsg.line - 1,
                            message: errorMsg.message,
                        });
                    }
                }
            }
            const diagCollection = hx.languages.createDiagnosticCollection(pluginName);
            diagCollection.set(doc.uri, errorMsgs);
            // 自动修复
            if (result.output !== undefined) {
                const diffs = diff.stringDiff(text, result.output, false);
                const workspaceEdit = new WorkspaceEdit();
                const edits = [];
                for (const df of diffs) {
                    edits.push(
                        new TextEdit(
                            {
                                start: df.originalStart,
                                end: df.originalStart + df.originalLength,
                            },
                            result.output.substr(df.modifiedStart, df.modifiedLength)
                        )
                    );
                }
                workspaceEdit.set(doc.uri, edits);
                hx.workspace.applyEdit(workspaceEdit);
            }
        }
        let level = 'info';
        const text2 = localize(1, null, pluginName, errorMsgs.length);
        const text3 = localize(2, null);
        const text4 = localize(3, null);
        let statusMsg = text2;
        let showtime = 5 * 1000;
        if (errorMsgs.length > 0) {
            level = 'error';
            statusMsg += text3;
            showtime = 30 * 1000;
        } else {
            if (isPathIgnore) {
                level = 'warn';
                statusMsg = text4;
                showtime = 5 * 1000;
            }
        }
        hx.window.setStatusBarMessage(statusMsg, showtime, level);
    }, settings);
}
// 处理错误
async function printError(err) {
    //  创建hx底部问题提示
    const errText = localize(4, null, pluginName);
    hx.window.setStatusBarMessage(errText, 2000, 'error');
    const view = hx.window.createOutputChannel('mxp-eslint-vue');
    const stack = err.stack.split(/(\r?\n)|\r/); // console.log(stack);
    // view.show();
    for (let i = 0, len = stack.length; i < len; i++) {
        if (/\S/.test(stack[i])) {
            const lineContent = {};
            lineContent.line = stack[i];
            lineContent.level = 'error';
            if (i === 0) {
                const isPath = path.isAbsolute(stack[i]);
                if (isPath) {
                    const lastIndex = stack[i].lastIndexOf(':');
                    const len1 = stack[i].length;
                    const filePath = stack[i].substr(0, lastIndex);
                    const lineNum = parseInt(stack[i].substr(lastIndex + 1, len1)) - 1;
                    lineContent.line = filePath;
                    lineContent.hyperlinks = [
                        {
                            linkPosition: {
                                start: 0,
                                end: stack[i].length,
                            },
                            async onOpen() {
                                const documentPromise = await hx.workspace.openTextDocument(filePath);
                                if (documentPromise) {
                                    const activeEditor = await hx.window.getActiveTextEditor();
                                    console.log(lineNum);
                                    activeEditor.gotoLine(lineNum);
                                }
                            },
                        },
                    ];
                }
            }
            // eslint-disable-next-line no-await-in-loop
            await view.appendLine(lineContent);
        }
    }
}
function activate(context) {
    console.log('run');
    const appRoot = hx.env.appRoot;
    const pluginVuePath = `${appRoot}/plugins/mxp-eslint-vue/`;
    extensionDir = context.extensionPath || pluginVuePath;
    const autoFixCommandDispose = hx.commands.registerCommand('extension.mxp-eslint-vue.autoFix', () => {
        const editorPromise = hx.window.getActiveTextEditor();
        editorPromise.then((editor) => {
            const doc = editor.document;
            try {
                validateAndFixDocument(doc, false);
            } catch (err) {
                // hx.window.showInformationMessage(err.stack);
                // let outputChannel = hx.window.createOutputChannel("foo");
                // outputChannel.show();
                // outputChannel.appendLine("Hello World");
            }
        });
    });
    const eslintConfig = hx.workspace.getConfiguration('mxp-eslint-vue');
    // 这个是执行保存格式化的时候用的
    const willSaveTextDocumentDispose = hx.workspace.onWillSaveTextDocument((event) => {
        const autoFixOnSave = eslintConfig.get('autoFixOnSave', true);
        try {
            validateAndFixDocument(event.document, autoFixOnSave);
        } catch (err) {
            //  创建hx底部问题提示
            printError(err);
        }
    });
    const didChangeTextDocumentDispose = hx.workspace.onDidChangeTextDocument((event) => {
        const validateOnDocumentChanged = eslintConfig.get('validateOnDocumentChanged');
        if (validateOnDocumentChanged) {
            validateAndFixDocument(event.document, false);
        }
    });

    context.subscriptions.push(autoFixCommandDispose);
    context.subscriptions.push(willSaveTextDocumentDispose);
    context.subscriptions.push(didChangeTextDocumentDispose);
}
module.exports = {
    activate,
};
