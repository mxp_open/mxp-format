# mxp-prettier

## 简介

此插件用于格式化 less、sass、vue、stylus、ts、yaml 代码，

### 格式化所有文件

```json
{
  "name": "mxp",
  "version": "1.0.0",
  "description": "",
  "main": "main.js",
  "scripts": {
    "install": "npm install",
    "prettier": "prettier --write ./**/*.{js,json,vue,ts,jsx,cjs,mjs,tsx,cts,mts,wxss,wxml,wxs}", // 使用prettier格式化所有文件
    "eslint": "eslint . --ext .vue,.js,.jsx,.cjs,.mjs,.ts,.tsx,.cts,.mts --fix", // 使用eslint格式化所有文件
    "stylelint": "stylelint **/*.{css,scss,vue} --fix" // 使用stylelint格式化所有文件
  },
  "devDependencies": {
    "eslint-plugin-html": "^7.1.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint": "^8.34.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint-plugin-jsdoc": "^40.0.0", // 用于eslint格式化jsdoc注释的 （如不需要可以不安装）
    "eslint-plugin-vue": "^9.9.0", // 用于eslint格式化vue （如不需要可以不安装）
    "postcss": "^8.4.21", // 用于stylelint格式化scss或者less建议安装 （如不需要可以不安装）
    "postcss-html": "^1.5.0", // 用于stylelint格式化html或者vue文件中的样式 （如不需要可以不安装）
    "prettier": "^2.8.4", // prettier 代码格式
    "sass": "^1.58.0", //  用于stylelint格式化(防止出错建议需要stylelint的也安装上)  （如不需要可以不安装）
    "stylelint": "15.1.0", //用于stylelint格式化 （如不需要可以不安装）
    "stylelint-config-html": "^1.1.0", // 用于stylelint格式化html或者vue文件中的样式（如不需要可以不安装）
    "stylelint-config-standard-scss": "^7.0.0", // stylelint的scss格式化规则 （如不需要可以不安装）
    "stylelint-order": "^6.0.2" // stylelint的scss属性排序规则 （如不需要可以不安装）
  }
}
```

1. 在根目录中新建 package.json，并加入上述 json 内容
2. 运行 -> 运行到终端 -> install （安装环境 如已安装请忽略）
3. 运行 -> 运行到终端 -> prettier （开始使用 prettier 格式化所有文件）

【vscode】：vscode 中怎么使用大家应该都知道，所以不做说明。

## 特别说明

使用 mxp 格式化全家桶：[mxp-stylelint](https://ext.dcloud.net.cn/plugin?id=10860)、[mxp-prettier](https://ext.dcloud.net.cn/plugin?id=10850)、[mxp-eslint-vue](https://ext.dcloud.net.cn/plugin?id=10851)、[mxp-eslint-js](https://ext.dcloud.net.cn/plugin?id=10853) 可无缝切换至 vscode 开发，适合即用 vscode 又用 HBuilder 的用户。（只需要在 vscode 中 npm 安装对应插件并拷贝对应规则即可（mxp-eslint-vue 和 mxp-eslint-js 是的插件和规则是一样的直接使用 mxp-eslint-js 的规则就行了）），这样就不用担心 vscode 和 HBuilder 的格式化规则不一致的问题了

## 修改或添加规则

点击 工具 -> 插件配置 -> mxp-prettier -> `prettier.config.js`，即可打开配置文件，在里面可以修改规则。

【注意】：由于 `HBuilder` 限制,在项目根目录配置`prettier.config.js`无效,只能在系统的配置文件里修改 (原因已知但无法修复，HBuilder 官方的`prettier`插件同样如此)

## 配置忽略文件或者文件夹（禁止格式化的文件或者文件夹）

1. 在项目根目录新建`.prettierignore`文件
2. 填写需要忽略的文件或者文件夹

```ignore
node_modules/*
uni_modules/*
build/*
dist/*
unpackage/*
static/*
*.min.js
*/*.min.js
```

## 如何配置格式化选项？

格式化选项配置文件是`prettier.config.js`,里面对应的选项说明如下：

```js
module.exports = {
  // 一行最多可以有多少个字符
  printWidth: 160,
  // 一个tab相当于多少个空格
  tabWidth: 2,
  // 是否使用使用tab缩进
  useTabs: false,
  // 是否在每个语句的末尾添加分号
  semi: true,
  // 使用单引号而不是双引号。
  singleQuote: true,
  // 在JSX中使用单引号而不是双引号
  jsxSingleQuote: false,
  // 对象的key是否用引号括起来  "as-needed" - 仅在需要时在对象属性周围添加引号   "consistent" - 如果对象中的至少一个属性需要加引号，就对所有属性加引号   "preserve" - 按照对象属性中引号的输入用法
  quoteProps: 'consistent',
  // 若为true，文件顶部加了 /** @prettier */或/** @format */的文件才会被格式化
  requirePragma: false,
  // Prettier可以在文件的顶部插入一个 @format的特殊注释，以表明改文件已经被Prettier格式化过了。在使用 --require-pragma参数处理一连串的文件时这个功能将十分有用。如果文件顶部已经有一个doclock，这个选项将新建一行注释，并打上@format标记。
  insertPragma: false,
  // 超过最大宽度是否换行 "always" - 如果超过最大宽度，请换行。 "never" - 不要换行。  "preserve" - 按原样显示。首先在v1.9.0中提供
  proseWrap: 'preserve',
  // 在单个箭头函数参数周围加上括号    avoid" - 尽可能省略parens。例：x => x  "always" - 始终包括parens。例：(x) => x
  arrowParens: 'always',
  // 在对象，数组括号与文字之间加空格 "{ foo: bar }"
  bracketSpacing: true,
  // 置统一的行结尾换行符  "lf" – 仅换行(\n),在Linux和macOS以及git repos内部通用 "crlf" - 回车符+换行符(\r\n)在Windows上很常见  "cr" - 仅回车符(\r)，很少使用   "auto" - 保持现有的行尾（通过查看第一行后的内容对一个文件中的混合值进行归一化）
  endOfLine: 'auto',
  // 是否显示HTML文件中的空格。 有效选项： 'css' -遵循 CSS 属性的默认值。 'strict' - 所有标签周围的空格（或缺少空格）被认为是重要的。 'ignore' - 所有标签周围的空格（或缺少空格）被认为是微不足道的
  htmlWhitespaceSensitivity: 'ignore',
  // 如果为true则把'>'放在末尾而不是单独放一行
  bracketSameLine: false,
  // 是否格式化嵌入在文件中的带引号的代码
  embeddedLanguageFormatting: 'auto',
  // 在 HTML、Vue 和 JSX 中每行强制实施单个属性
  singleAttributePerLine: false,
  // 在数组对象等末尾添加逗号  "none" - 没有尾随逗号。 "es5" - 在ES5中有效的尾随逗号（对象，数组等）  "all" - 尽可能使用尾随逗号（包括函数参数）。这需要 nodejs 8
  trailingComma: 'es5',
  // 是否给vue中的 <script>和<style>标签加缩进
  vueIndentScriptAndStyle: true,
};
```

详细的配置说明可以参考[options](https://prettier.io/docs/en/options.html)

## 在 vscode 或者 HBuilder 中使用 mxp 格式化全家桶

```json
{
  "name": "mxp",
  "version": "1.0.0",
  "description": "",
  "main": "main.js",
  "scripts": {
    "install": "npm install",
    "prettier": "prettier --write ./**/*.{js,json,vue,ts,jsx,cjs,mjs,tsx,cts,mts,wxss,wxml,wxs}",
    "eslint": "eslint . --ext .vue,.js,.jsx,.cjs,.mjs,.ts,.tsx,.cts,.mts --fix", // 使用eslint格式化所有文件
    "stylelint": "stylelint **/*.{css,scss,vue} --fix" // 使用stylelint格式化所有文件
  },
  "devDependencies": {
    "@vue/eslint-config-typescript": "^11.0.2", // 用于eslint格式化ts
    "eslint-plugin-html": "^7.1.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint": "^8.34.0", // 用于eslint格式化vue （如不需要可以不安装）
    "eslint-plugin-jsdoc": "^40.0.0", // 用于eslint格式化jsdoc注释的 （如不需要可以不安装）
    "eslint-plugin-vue": "^9.9.0", // 用于eslint格式化vue （如不需要可以不安装）
    "postcss": "^8.4.21", // 用于stylelint格式化scss或者less建议安装 （如不需要可以不安装）
    "postcss-html": "^1.5.0", // 用于stylelint格式化html或者vue文件中的样式 （如不需要可以不安装）
    "prettier": "^2.8.4", // prettier 代码格式
    "sass": "^1.58.0", //  用于stylelint格式化(防止出错建议需要stylelint的也安装上)  （如不需要可以不安装）
    "stylelint": "15.1.0", //用于stylelint格式化 （如不需要可以不安装）
    "stylelint-config-html": "^1.1.0", // 用于stylelint格式化html或者vue文件中的样式（如不需要可以不安装）
    "stylelint-config-standard-scss": "^7.0.0", // stylelint的scss格式化规则 （如不需要可以不安装）
    "stylelint-order": "^6.0.2" // stylelint的scss属性排序规则 （如不需要可以不安装）
  }
}
```

1. 在根目录中新建 package.json，并加入上述 json
2. 运行 -> 运行到终端 -> install （安装环境 【注意如果想实现保存时自动格式化还是需要在插件市场下载此插件】）
3. 运行 -> 运行到终端 -> eslint eslint 格式化）

【vscode】：vscode 中怎么使用大家应该都知道，所以不做说明。

## 禁止自带的格式化工具 (js-beautify)

HBuilder 默认使用`js-beautify`格式化代码且不可卸载，当同时存在`js-beautify`和`prettier`插件时，`prettier`格式化`.vue`,`prettier`格式化,`js-beautify`格式化`.js`(详见：[HBuilderX 格式化操作、及格式化插件配置说明](https://ask.dcloud.net.cn/article/36529)),这样会导致 js 和 vue 不是用的同一个格式化工具，所以建议删除自带
`js-beautify`格式化工具，或者禁止使用`js-beautify`格式化，具体操作如下：

1. `HBuilder X`中点击 工具 -> 插件配置 -> jsbeautify -> 打开 jsbeautifyrc.js 进行配置。
2. 在配置文件中屏蔽 `parsers` 节点中的键值对，如图：

```js
module.exports = {
  options: {
    indent_size: '1',
    indent_char: '\t',
    // 由于篇幅太长下面的省略
  },
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  // 【重点】 删除或者屏蔽 parsers 里面的键值对
  parsers: {
    // '.js': 'js',
    // '.json': 'js',
    // '.njs': 'js',
    // '.sjs': 'js',
    // '.wxs': 'js',
    // '.css': 'css',
    // '.nss': 'css',
    // '.wxss': 'css',
    // 由于篇幅太长下面的省略
  },
};
```

## 问题解答

### 明明下载的时最新版规则还是旧版的

由于用户的配置文件(工具 -> 设置 -> 插件配置 -> 打开对应的配置文件)会覆盖自带的配置文件，所以建议先备份后删除原配置文件以及他的上级目录，然后重新打开 工具 -> 设置 -> 插件配置 -> 对应的配置文件 ，此时会生成一个最新版的配置文件

### 修改项目根目录的配置文件不生效

本插件根目录的配置文件仅用于格式化所有文件时使用，在保存时验证请直接修改 工具 -> 设置 -> 插件配置 -> 中的配置文件

### 其他问题

还有问题请在[本插件评论区](https://ext.dcloud.net.cn/plugin?id=10850)或者提交[issues](https://gitee.com/mxp131011/mxp-format/issues)

## 感谢

本插件是基于[DCloud-HBuilderX](https://ext.dcloud.net.cn/publisher?id=37996) 团队制作的 [prettier](https://ext.dcloud.net.cn/plugin?id=2025)插件的一个升级版,在此对原作者表示感谢
